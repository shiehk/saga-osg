#!/usr/bin/env python

"""
Script for remotely submitting single selex_covarianceSearch jobs to the Open
Science Grid (OSG).

Requirements: DMTCP 1.2.8
HTCondor < 8 if running DMTCP (see http://dmtcp.sourceforge.net/condor.html)

  Notes:
    1. Options for condor_submit can be added to the submit_opts dictionary
    2. The last part of the code closes the job service and then deletes the
       files left in the OSG home directory that we transferred.
    3. If you are running DMTCP, make sure all the DMTCP binaries and libraries
       are located in the main folder.
    4. Strongly recommended: Use SSH's key-based authentication to connect,
       bypassing the need to type the password.
       https://help.ubuntu.com/community/SSH/OpenSSH/Keys
       ssh-keygen -t rsa -b 4096
       ssh-copy-id <username>@<host>
    5. If the SSH connection fails and causes SAGA to stop running, then set
       the timeout to 60 or 120.
       export SAGA_PTY_SSH_TIMEOUT=60
"""

import saga
import sys
import time

"""
Configuration

Fill out the information below.
"""

# Files, paths, and parameters
prefix = 'cluster_2_top'            # Prefix for file names
rounds = 10                         # Number of rounds to run
files = {'cm': prefix + '.cm',
         'fasta': prefix + '.fasta',
         'sto': prefix + '.sto',
         'config': 'cluster.cfg'}

# Project-specific configuration options
project_name = 'ConnectTrain'
time_limit = 600                    # Time limit, in minutes
poll_interval = 10                  # Interval to poll HTCondor, in minutes

# Run DMTCP?
# Note: The DMTCP shim script does not work on HTCondor >= 8.0!
run_dmtcp = 0                       # DMTPC: 0 = disable, 1 = enable

# Set up SAGA and HTCondor
user_id = ''
osg_url = 'login.osgconnect.net'    # URL for OSG gateway
osg_home = '/home/username/'        # Path to home directory on OSG gateway
notification = ''                   # Always or Never
notification_email = ''


"""
Main function
"""

def main():
    
    try:
        # SSH identity on the remote machine
        ctx = saga.Context("ssh")
        ctx.user_id = user_id
        session = saga.Session()
        session.add_context(ctx)
        
        # Create a job service. We can pass condor_submit options via the query
        # component of the job service URL. For additional options, see manual:
        # http://research.cs.wisc.edu/htcondor/manual/current/condor_submit.html
        submit_opts = {'when_to_transfer_output': 'ON_EXIT',
                       'should_transfer_files': 'YES',
                       'notification': notification,
                       'notify_user': notification_email}
        submit_list = list()
        for k, v in submit_opts.iteritems():
            submit_list.append("%s=%s" % (k, v))
        condor_url = 'condor+ssh://' + osg_url + '?' + '&'.join(submit_list)
        js = saga.job.Service(condor_url, session=session)
        
        # Describe our job environment, executable, and arguments
        jd = saga.job.Description()
        jd.name                 = 'selex'
        jd.project              = project_name
        jd.wall_time_limit      = time_limit # Minutes
        jd.environment          = {}    # Dictionary of environment variables
        jd.working_directory    = ''    # Needed for newer versions of SAGA
        jd.executable           = '/bin/bash' # Executable
        jd.arguments            = ['selex_covarianceSearch.sh', files['cm'],
                                   files['fasta'], files['sto'], rounds,
                                   files['config'], prefix] # List of arguments
        jd.output               = "saga_" + prefix + ".out"
        jd.error                = "saga_" + prefix + ".err"
        
        outputfile              = prefix + '.sh'
        if run_dmtcp:
            jd.file_transfer    = [
                "selex_covarianceSearch.sh > selex_covarianceSearch.sh",
                "%s > %s" % (files['cm'], files['cm']),
                "%s > %s" % (files['fasta'], files['fasta']),
                "%s > %s" % (files['sto'], files['sto']),
                "%s > %s" % (files['config'], files['config']),
                "%s > %s" % ('exec.tar.gz', 'exec.tar.gz'),
                "%s > %s" % ('dmtcp_checkpoint', 'dmtcp_checkpoint'),
                "%s > %s" % ('dmtcp_command', 'dmtcp_coordinator'),
                "%s > %s" % ('dmtcp_coordinator', 'dmtcp_coordinator'),
                "%s > %s" % ('dmtcphijack.so', 'dmtcphijack.so'),
                "%s > %s" % ('dmtcp_restart', 'dmtcp_restart'),
                "%s > %s" % ('libmtcp.so', 'libmtcp.sp'),
                "%s > %s" % ('libmtcp.so.1', 'libmtcp.so.1'),
                "%s > %s" % ('mtcp_restart', 'mtcp_restart'),
                "%s > %s" % ('shim_dmtcp', 'shim_dmtcp'),
                "%s < %s" % (prefix + '.sh', prefix + '.sh'),
                "%s < %s" % (prefix + '_dir.tar.gz', prefix + '_dir.tar.gz')]
        else:
            jd.file_transfer   = [
                "selex_covarianceSearch.sh > selex_covarianceSearch.sh",
                "%s > %s" % (files['cm'], files['cm']),
                "%s > %s" % (files['fasta'], files['fasta']),
                "%s > %s" % (files['sto'], files['sto']),
                "%s > %s" % (files['config'], files['config']),
                "%s > %s" % ('exec.tar.gz', 'exec.tar.gz'),
                "%s < %s" % (prefix + '.sh', prefix + '.sh'),
                "%s < %s" % (prefix + '_dir.tar.gz', prefix + '_dir.tar.gz')]
       
        # Create the job (state: New), then start it and check its id and state
        job = js.create_job(jd)
        print("\n%s - ... Starting job ...\n" % time.asctime())
        job.run()
        print("Job ID      : %s" % job.id)
        print("Job State   : %s" % job.state)
        
        # Disconnect/reconnect, which is useful for long-running jobs
        job_clone = js.get_job(job.id)
        
        # Wait for the job to complete
        print("\n%s - ... Waiting for job ...\n" % time.asctime())
        #job_clone.wait(time_limit * 60.0) # Timeout in seconds (floating point)
        _job_wait_custom(job_clone, poll_interval * 60.0, time_limit * 60.0)
        exit_code = job_clone.exit_code
        
        print("Job State   : %s" % job_clone.state)
        print("Exitcode    : %s" % job_clone.exit_code)
        print("Create time : %s" % job_clone.created)
        print("Start time  : %s" % job_clone.started)
        print("End time    : %s" % job_clone.finished)
        print("Running time: %s minutes" % round((int(job_clone.finished) -
                                                  int(job_clone.started)) / 60,
                                                 2))
        
        js.close() # Close session to allow files to be transferred
        
        # Verify that job executed correctly, then clean up the home directory
        # on OSG
        if exit_code == 0:
            # List of files transferred
            file_list = map(lambda x: x.split()[0], jd.file_transfer)
            for fname in file_list:
                fh = saga.filesystem.File(saga.Url('sftp://' + osg_url +
                                                   osg_home + fname))
                fh.remove()
        print("\n%s - SAGA completed" % time.asctime())
        
        return 0
    
    # Catch all exceptions and show traceback for debugging
    except saga.SagaException, ex:
        print("An exception occured: %s " % str(ex))
        print(" \n*** Backtrace:\n %s" % ex.traceback)
        sys.exit(-1) 
    
    # Ctrl-C caught; cancel all jobs before exiting the program
    except KeyboardInterrupt:
        for job in jobs:
            job.cancel()
        sys.exit(-1)

def _job_wait_custom(job, poll_time, timeout):
    """
    Modified version of the _job_wait() function in condorjob.py with the
    ability to customize the polling interval. By default, the function polls
    every 0.5 seconds, which could be slow on OSG.
    """
    time_start = time.time()
    time_now   = time_start
    
    while True:
        state = job.get_state()
        
        if state == saga.job.DONE or \
           state == saga.job.FAILED or \
           state == saga.job.CANCELED:
            return True
        else:
            print(" * Job %s status: %s" % (job.id, state))
        
        # Set poll interval
        time.sleep(poll_time)
        
        # Check if we hit timeout
        if timeout >= 0:
            time_now = time.time()
            if time_now - time_start > timeout:
                return False    

if __name__ == "__main__":
    sys.exit(main())
