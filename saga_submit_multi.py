#!/usr/bin/env python

"""
Script for remotely submitting multiple selex_covarianceSearch jobs to the Open
Science Grid (OSG).

Requirements: DMTCP 1.2.8
HTCondor < 8 if running DMTCP (see http://dmtcp.sourceforge.net/condor.html)

  Notes:
    1. Put names of all file prefixes in 'prefix_list'. The options for
       condor_submit can be added to the submit_opts dictionary.
    2. The last part of the code closes the job service and then deletes the
       files left in the OSG home directory that we transferred.
    3. If you are running DMTCP, make sure all the DMTCP binaries and libraries
       are located in the main folder.
    4. Strongly recommended: Use SSH's key-based authentication to connect,
       bypassing the need to type the password.
       https://help.ubuntu.com/community/SSH/OpenSSH/Keys
       ssh-keygen -t rsa -b 4096
       ssh-copy-id <username>@<host>
    5. If the SSH connection fails and causes SAGA to stop running, then set
       the timeout to 60 or 120.
       export SAGA_PTY_SSH_TIMEOUT=60
"""

import saga
import sys
import time
import os

"""
Configuration

Fill out the information below.
"""

# Files, paths, and parameters
prefix_list = ['cluster_1_top', 'cluster_2_top',    # List of file prefixes
               'cluster_3_top', 'cluster_4_top',
               'cluster_5_top', 'cluster_6_top',
               'cluster_7_top', 'cluster_8_top',
               'cluster_9_top', 'cluster_10_top']
rounds = 10                                         # Number of rounds to run
all_transferred_files = []
output_list = []

# Project-specific configuration options
project_name = 'ConnectTrain'
time_limit = 600                    # Time limit, in minutes
poll_interval = 10                  # Interval to poll HTCondor, in minutes

# Run DMTCP?
# Note: The DMTCP shim script does not work on HTCondor >= 8.0!
run_dmtcp = 0                       # DMTPC: 0 = disable, 1 = enable

# Set up SAGA and HTCondor
user_id = ''
osg_url = 'login.osgconnect.net'    # URL for OSG gateway
osg_home = '/home/username/'        # Path to home directory on OSG gateway
notification = 'Always'             # Always or Never
notification_email = ''


"""
Main function
"""

def main():

    try:
        # SSH identity on the remote machine
        ctx = saga.Context("ssh")
        ctx.user_id = user_id
        session = saga.Session()
        session.add_context(ctx)

        # Create a job service. We can pass condor_submit options via the query
        # component of the job service URL. For additional options, see manual:
        # http://research.cs.wisc.edu/htcondor/manual/current/condor_submit.html
        submit_opts = {'when_to_transfer_output': 'ON_EXIT',
                       'should_transfer_files': 'YES',
                       'notification': notification,
                       'notify_user': notification_email}
        submit_list = list()
        for k, v in submit_opts.iteritems():
            submit_list.append("%s=%s" % (k, v))
        condor_url = 'condor+ssh://' + osg_url + '?' + '&'.join(submit_list)
        js = saga.job.Service(condor_url, session=session)
        
        jobs = list()
        start_time = time.time()
        print("\n%s - ... Starting job ...\n" % time.asctime())
        
        for pref in prefix_list:
            # Describe each job environment, executable, and arguments
            jd = saga.job.Description()
            jd.name            = 'Multiple SELEX Job'
            jd.project         = project_name
            jd.wall_time_limit = time_limit # Minutes
            jd.environment     = {}         # Dictionary of environment variables
            jd.working_directory    = ''    # Needed for newer versions of SAGA
            jd.executable      = '/bin/bash' # Executable
            jd.arguments       = ['selex_covarianceSearch.sh', pref+'.cm',
                                  pref+'.fasta', pref+'.sto', rounds,
                                  'cluster.cfg', pref] # List of arguments
            jd.output          = 'saga_' + pref + '_job.out'
            jd.error           = 'saga_' + pref + '_job.err'
            
            outputfile         = pref + '_dir.tar.gz'
            
            if run_dmtcp:
                jd.file_transfer   = [
                    "selex_covarianceSearch.sh > selex_covarianceSearch.sh",
                    "%s > %s" % (pref + '.cm', pref + '.cm'),
                    "%s > %s" % (pref + '.fasta', pref + '.fasta'),
                    "%s > %s" % (pref + '.sto', pref + '.sto'),
                    "%s > %s" % ('cluster.cfg', 'cluster.cfg'),
                    "%s > %s" % ('exec.tar.gz', 'exec.tar.gz'),
                    "%s > %s" % ('dmtcp_checkpoint', 'dmtcp_checkpoint'),
                    "%s > %s" % ('dmtcp_command', 'dmtcp_coordinator'),
                    "%s > %s" % ('dmtcp_coordinator', 'dmtcp_coordinator'),
                    "%s > %s" % ('dmtcphijack.so', 'dmtcphijack.so'),
                    "%s > %s" % ('dmtcp_restart', 'dmtcp_restart'),
                    "%s > %s" % ('libmtcp.so', 'libmtcp.sp'),
                    "%s > %s" % ('libmtcp.so.1', 'libmtcp.so.1'),
                    "%s > %s" % ('mtcp_restart', 'mtcp_restart'),
                    "%s > %s" % ('shim_dmtcp', 'shim_dmtcp'),                                      
                    "%s < %s" % (pref + '.sh', pref + '.sh'),
                    "%s < %s" % (pref + '_dir.tar.gz', pref + '_dir.tar.gz')]
            else:
                jd.file_transfer = [
                    "selex_covarianceSearch.sh > selex_covarianceSearch.sh",
                    "%s > %s" % (pref + '.cm', pref + '.cm'),
                    "%s > %s" % (pref + '.fasta', pref + '.fasta'),
                    "%s > %s" % (pref + '.sto', pref + '.sto'),
                    "%s > %s" % ('cluster.cfg', 'cluster.cfg'),
                    "%s > %s" % ('exec.tar.gz', 'exec.tar.gz'),                                    
                    "%s < %s" % (pref + '.sh', pref + '.sh'),
                    "%s < %s" % (pref + '_dir.tar.gz', pref + '_dir.tar.gz')]
            # Add list of files to all_transferred_files list
            # Don't transfer 'selex_covarianceSearch.sh', 'cluster.cfg', and
            # 'exec.tar.gz', which will be added later to avoid duplicates.
            file_list = map(lambda x: x.split()[0], jd.file_transfer)
            for i in file_list:
                if ((i != 'selex_covarianceSearch.sh') and
                    (i != 'cluster.cfg') and (i != 'exec.tar.gz')):
                    all_transferred_files.append(i)
            # Create the job (state: New)
            job = js.create_job(jd)
            job.run()
            jobs.append(job)
            output_list.append(outputfile)
            print(" * Submitted %s. Output will be written to: %s" %
                  (job.id, outputfile))
        
        # Wait for all jobs to finish. You can change how long the program
        # sleeps before checking the job state again.
        while len(jobs) > 0:
            for job in jobs:
                jobstate = job.get_state()
                print(" * Job %s status: %s" % (job.id, jobstate))
                if jobstate in [saga.job.DONE, saga.job.FAILED]:
                    print(" * Job %s running time: %s minutes" 
                          % (job.id, round((int(job.finished) -
                                            int(job.started)) / 60, 2)))
                    jobs.remove(job)
                time.sleep(poll_interval * 60.0)
        
        # Calculate run time
        run_time = time.time() - start_time
        print("Total running time: %s minutes" % int(run_time / 60))
        js.close() # Close session to allow files to be transferred

        # Check to ensure that all files are transferred over, and then clean up
        # the home directory on OSG
        files_copied = True
        for out in output_list:
            if os.path.isfile(out) == False:
                files_copied = False
        if files_copied:
            all_transferred_files.append('selex_covarianceSearch.sh')
            all_transferred_files.append('cluster.cfg')
            all_transferred_files.append('exec.tar.gz')
            for fname in all_transferred_files:
                fh = saga.filesystem.File(saga.Url('sftp://' + osg_url +
                                                   osg_home + fname))
                fh.remove()
        else:
            print("Warning: Some output files were not transferred!")
            return -1
        
        print("\n%s - SAGA completed" % time.asctime())
        
        return 0
    
    # Catch all exceptions and show traceback for debugging
    except saga.SagaException, ex:
        print("An exception occured: (%s) %s " % (ex.type, (str(ex))))
        print(" \n*** Backtrace:\n %s" % ex.traceback)
        print(time.asctime())
        sys.exit(-1) 
    
    # Ctrl-C caught; cancel all jobs before exiting the program
    except KeyboardInterrupt:
        for job in jobs:
            job.cancel()
        sys.exit(-1) 

if __name__ == "__main__":
    sys.exit(main())
