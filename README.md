# README

These are files to run covariance model (CM) refinement on the Open Science
Grid (OSG) using SAGA-Python and DMTCP for checkpointing.

There are two files, `saga_submit.py` and `saga_submit_multi.py`. The first runs
CM refinement on one CM. The second file submits jobs in parallel.

Usage:

1. Edit the configuration at the top of the file
2. `python <file>`
